import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { ProdutsComponent } from './produts/produts.component';
import { RegisterComponent } from './register/register.component';
import { TableComponent } from './table/table.component';

// landing child routes
const landingChildRoutes : Routes = [
  {
    path: 'user',
    component: TableComponent
  },
  {
    path: 'produts',
    component: ProdutsComponent
  }
];

// main routes
const routes: Routes = [
  {
    path:"",component: LoginComponent
  },
  {
    path:"login",component: LoginComponent
  },
  {
    path:"register",component: RegisterComponent
  },
  {
    path:"landing",component: LandingComponent, children:landingChildRoutes
  }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
