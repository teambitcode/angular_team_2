import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main-service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  password: string = "";
  email: string = "";
  constructor(private router: Router, private mainService: MainService) { }

  ngOnInit(): void {
  }
  navigateToRegister() {
    this.router.navigateByUrl("register");
  }

  login() {
    let tempBody = {
      "email": this.email,
      "password": this.password
    };
    // this.router.navigateByUrl("landing");
    this.mainService.postApi('/user/login', tempBody).subscribe((response: any) => {
      if (response.status) {
        Swal.fire({
          position: 'top-start',
          icon: 'success',
          title: 'User Logged Successfuly',
          showConfirmButton: false,
          timer: 2500
        });
        this.router.navigateByUrl("landing");
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        })
      }
    }, (error: any) => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      })
    });
  }
}
