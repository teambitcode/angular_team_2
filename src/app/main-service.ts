import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
@Injectable({
    providedIn: 'root',
})
export class MainService {

    constructor(private http: HttpClient){
    }

    getApi(path: string): Observable<any> {
        return this.http.get(environment.basePath+path);
    }
    
    postApi(path: string, body:any): Observable<any> {
        return this.http.post(environment.basePath+path,body);
    }

}