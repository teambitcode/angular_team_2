import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main-service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  email: string = "";
  password: string = "";
  confirmPassword: string = "";
  firstName: string = "";
  lastName: string = "";

  constructor(private router: Router, private mainService: MainService) { }

  ngOnInit(): void {
  }
  navigateToLogin() {
    this.router.navigateByUrl("login");
  }
  registerUser() {
    let body = {
      "role": "user",
      "email": this.email,
      "password": this.password,
      "first_name": this.firstName,
      "last_name": this.lastName,
    };

    this.mainService.postApi('/user/new', body).subscribe((response: any) => {
      if (response.status) {
        Swal.fire({
          position: 'top-start',
          icon: 'success',
          title: 'User Created Successfuly',
          showConfirmButton: false,
          timer: 2500
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        })
      }
    },(error:any)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      })
    });
  }

}
