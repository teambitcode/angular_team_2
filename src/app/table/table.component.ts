import { Component, OnInit } from '@angular/core';
import { MainService } from '../main-service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  userArray: any[] = [];
  constructor(private mainService: MainService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.mainService.getApi('/user/getAll').subscribe((response: any) => {
      this.userArray = response.data;
    });
  }

}
